/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost
 Source Database       : goods

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : utf-8

 Date: 07/26/2018 14:09:11 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `category_`
-- ----------------------------
DROP TABLE IF EXISTS `category_`;
CREATE TABLE "category_" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "name" varchar(50) DEFAULT NULL,
  "version" int(11) DEFAULT '1',
  PRIMARY KEY ("id")
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `category_`
-- ----------------------------
BEGIN;
INSERT INTO `category_` VALUES ('1', '333', '1');
COMMIT;

-- ----------------------------
--  Table structure for `tb_dept`
-- ----------------------------
DROP TABLE IF EXISTS `tb_dept`;
CREATE TABLE "tb_dept" (
  "Id" int(11) NOT NULL AUTO_INCREMENT,
  "Name" varchar(18) DEFAULT NULL,
  "description" varchar(100) DEFAULT NULL,
  PRIMARY KEY ("Id")
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tb_dept`
-- ----------------------------
BEGIN;
INSERT INTO `tb_dept` VALUES ('1', '经营部', '修改后2'), ('2', '设备部', '修理啊'), ('3', '开发部', '码农啊'), ('4', '财务部', '财务部啊');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE "user" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "address" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "age" int(11) DEFAULT NULL,
  "email" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "name" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "password" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "sex" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "UK_gj2fy3dcix7ph7k8684gka40c" ("name")
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('5', null, '30', 'guanyu@shu.com.cn', '关羽', '123', null), ('6', null, '30', 'zhangfei@shu.com.cn', '张飞', '123', null), ('7', null, '30', 'zhaoyun@shu.com.cn', '赵云', '123', null), ('8', '允州', '30', 'xiahoudun@sanguo.com.cn', '夏侯惇', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
