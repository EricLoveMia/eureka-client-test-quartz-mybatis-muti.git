package com.hotkidceo.springcloud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotkidceo.springcloud.dao.bpm.UserDao;
import com.hotkidceo.springcloud.domain.bpm.UserDO;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;
    
    public int addUser(UserDO user){
        return userDao.insert(user);
    }
    
    public UserDO getUserById(Long id){
        return userDao.selectByPrimaryKey(id);
    }
    
    public List<UserDO> getAllUsers(){
        return userDao.getAllUsers();
    }
}
