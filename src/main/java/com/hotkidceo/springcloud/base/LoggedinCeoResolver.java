package com.hotkidceo.springcloud.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.hotkidceo.springcloud.domain.Ceo;

/**
 * ceo
 */
public class LoggedinCeoResolver implements HandlerMethodArgumentResolver {

	@Override
	public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
		Ceo result = null;
		try {
			HttpServletRequest request = (HttpServletRequest) nativeWebRequest.getNativeRequest();
			if (request.getSession(false) != null) {
				result = (Ceo) request.getSession(false).getAttribute("ceo");
			}
		}
		catch (Exception e) {
		}
		return result;
	}

	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return methodParameter.getParameterType().equals(Ceo.class);
	}
}
