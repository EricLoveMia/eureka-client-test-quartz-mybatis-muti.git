package com.hotkidceo.springcloud.dao.goods;

import java.util.List;

import com.hotkidceo.springcloud.domain.goods.DeptDO;

public interface DeptDao {
	/**
	 * 
	* @Title: getAllUsers
	* @Description: 获取所有用户
	* @param @return    设定文件
	* @return List<UserDO>    返回类型
	* @throws
	 */
	List<DeptDO> getAllDepts();
	
	/**
	 * 
	* @Title: insert
	* @Description: 插入一个用户
	* @param @param record
	* @param @return    设定文件
	* @return int    返回类型
	* @throws
	 */
	int insert(DeptDO record);
	
	/**
	 * 
	* @Title: selectByPrimaryKey
	* @Description: 查询一个用户
	* @param @param id
	* @param @return    设定文件
	* @return UserDO    返回类型
	* @throws
	 */
	DeptDO selectByPrimaryKey(Long id);
}
