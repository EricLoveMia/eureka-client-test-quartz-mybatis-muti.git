package com.hotkidceo.springcloud.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
@MapperScan(basePackages = {"com.hotkidceo.springcloud.dao.goods"},sqlSessionFactoryRef="sqlSessionFactoryGoods")
public class MybatisDBGoodsConfig {
	@Autowired
	private Environment env;
	
	@Autowired
    @Qualifier("goodsDS")
    private DataSource goodsDS;
	
	@Bean
	public SqlSessionFactory sqlSessionFactoryGoods() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(goodsDS);
        //下边两句仅仅用于*.xml文件，如果整个持久层操作不需要使用到xml文件的话（只用注解就可以搞定），则不加
		factoryBean.setTypeAliasesPackage(env.getProperty("mybatisgoods.typeAliasesPackage"));
		factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(env.getProperty("mybatisgoods.mapperLocations")));
		return factoryBean.getObject();
	}
	
    @Bean
    public SqlSessionTemplate sqlSessionTemplateBPM() throws Exception {
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactoryGoods()); // 使用上面配置的Factory
        return template;
    }
}
