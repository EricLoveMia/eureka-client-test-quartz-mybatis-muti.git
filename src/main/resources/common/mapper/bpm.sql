/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost
 Source Database       : bpm

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : utf-8

 Date: 07/26/2018 14:08:57 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `hibernate_sequence`
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE "hibernate_sequence" (
  "next_val" bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `hibernate_sequence`
-- ----------------------------
BEGIN;
INSERT INTO `hibernate_sequence` VALUES ('8');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE "user" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "address" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "age" int(11) DEFAULT NULL,
  "email" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "name" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "password" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "sex" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "UK_gj2fy3dcix7ph7k8684gka40c" ("name")
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('5', null, '30', 'guanyu@shu.com.cn', '关羽', '123', null), ('6', null, '30', 'zhangfei@shu.com.cn', '张飞', '123', null), ('7', null, '30', 'zhaoyun@shu.com.cn', '赵云', '123', null), ('8', '允州', '30', 'xiahoudun@sanguo.com.cn', '夏侯惇', 'c4885b551bc4ec4699b4151b0aec67f7', null), ('9', '允州', '28', 'xiahouyuan@wei.com.cn', '夏侯渊', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
