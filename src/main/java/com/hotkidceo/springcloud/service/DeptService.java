package com.hotkidceo.springcloud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotkidceo.springcloud.dao.goods.DeptDao;
import com.hotkidceo.springcloud.domain.goods.DeptDO;

@Service
public class DeptService {

    @Autowired
    private DeptDao deptDao;
    
    public int addUser(DeptDO user){
        return deptDao.insert(user);
    }
    
    public DeptDO getUserById(Long id){
        return deptDao.selectByPrimaryKey(id);
    }
    
    public List<DeptDO> getAllUsers(){
        return deptDao.getAllDepts();
    }
}
