package com.hotkidceo.springcloud.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
@MapperScan(basePackages = {"com.hotkidceo.springcloud.dao.bpm"},sqlSessionFactoryRef="sqlSessionFactoryBPM")
public class MybatisDBBPMConfig {
	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("bpmDS")
    private DataSource bpm;
	
	@Bean
	@Primary
	public SqlSessionFactory sqlSessionFactoryBPM() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(bpm);
        //下边两句仅仅用于*.xml文件，如果整个持久层操作不需要使用到xml文件的话（只用注解就可以搞定），则不加
		factoryBean.setTypeAliasesPackage(env.getProperty("mybatisbpm.typeAliasesPackage"));
		factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(env.getProperty("mybatisbpm.mapperLocations")));
		return factoryBean.getObject();
	}
	
    @Bean
    @Primary
    public SqlSessionTemplate sqlSessionTemplateBPM() throws Exception {
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactoryBPM()); // 使用上面配置的Factory
        return template;
    }
}
