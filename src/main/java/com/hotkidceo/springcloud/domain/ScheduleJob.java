package com.hotkidceo.springcloud.domain;

/**
 * 
* @ClassName: ScheduleJob
* @Description: 任务调度对象
* @author 00322027
* @date 2018年5月9日 下午1:47:31
*
 */
public class ScheduleJob {

	private String jobName;
	private String jobGroup;
	private String desc;
	private String jobStatus;
	private String cronExpression;
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobGroup() {
		return jobGroup;
	}
	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	
	
}
