package com.hotkidceo.springcloud.controller;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hotkidceo.springcloud.domain.goods.DeptDO;
import com.hotkidceo.springcloud.service.DeptService;

@RestController
@RequestMapping("/dept")
public class DeptController {

	private static final Logger logger = LoggerFactory.getLogger(DeptController.class);

	@Autowired
	private DeptService deptService;

	@Resource
	private DiscoveryClient client;

	@RequestMapping(value = "/addDept", method = RequestMethod.POST)
	public int addUser() {
		DeptDO dept = new DeptDO();
		dept.setName("财务部");
		dept.setDescription("财务部啊");
		return deptService.addUser(dept);
	}

	@RequestMapping(value = "/getDeptById", method = RequestMethod.GET)
	public DeptDO getDeptById(@RequestParam Long id) {
		return deptService.getUserById(id);
	}

	@RequestMapping(value = "/getAllDepts", method = RequestMethod.GET)
	public List<DeptDO> getAllDepts() {
		List<DeptDO> listDept = deptService.getAllUsers();
		// 输出服务信息
		logger.info("result={}", listDept);
		return listDept;
	}
}
